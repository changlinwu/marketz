import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import { Row, Col } from 'view-ui-plus';
import 'swiper/css';
import 'swiper/css/pagination';
import 'view-ui-plus/dist/styles/viewuiplus.css';
// import './assets/scss/app.scss';

const app = createApp(App);

app.use(router)
app.use(store)
app.component('Row', Row)
app.component('Col', Col)
app.mount('#app')
