import { createStore } from 'vuex';

const store = createStore({
  state () {
    return {
      showHeader: true,
    }
  },
  mutations: {
    SET_SHOW_HEADER (state, value) {
      state.showHeader = value
    }
  },
  actions: {
    setShowHeader ({ commit, state }, value) {
      this.commit('SET_SHOW_HEADER', value);
    }
  },
  getters: {
    getShowHeader: (state) => state.showHeader
  }
});

export default store