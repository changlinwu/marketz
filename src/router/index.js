import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Mvp from '../views/Mvp.vue'
import Captainz from '../views/Captainz.vue'
import Potatoz from '../views/Potatoz.vue'
import Islandz from '../views/Islandz.vue'
import Profile from '../views/Profile.vue'

const routes = [
  { path: '/', name: 'Home', component: Home },
  { path: '/mvp', name: 'Mvp', component: Mvp },
  { path: '/captainz', name: 'Captainz', component: Captainz },
  { path: '/potatoz', name: 'Potatoz', component: Potatoz },
  { path: '/islandz', name: 'Islandz', component: Islandz },
  { path: '/profile', name: 'Profile', component: Profile },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router;